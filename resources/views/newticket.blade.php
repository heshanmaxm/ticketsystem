<x-master>
    @section('content')
        <h1 class="h3 mb-2 text-gray-800">Tickets</h1>


        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">
                    <a href="{{route('tickets.create')}}" class="btn btn-success">Open A New Ticket</a>
                </div>
                <div class="float-right">
                    <a href="{{url('/')}}" class="btn btn-primary">Check Ticket</a>
                </div>
            </div>
            <div class="card-body">
                @include('includes.flash')
                <div class="table-responsive">
                    <form method="POST" action="{{route('tickets.store')}}" enctype="multipart/form-data" id="upload_image_form">
                        @csrf
                        <div class="form-group">
                            <label for="client_name">Name </label>
                            <input type="text" class="form-control" id="client_name" name="client_name">
                        </div>
                        <div class="form-group">
                            <label for="client_email">Email </label>
                            <input type="email" class="form-control" id="client_email" name="client_email">
                        </div>
                        <div class="form-group">
                            <label for="client_phone">Phone</label>
                            <input type="text" class="form-control" id="client_phone" name="client_phone">
                        </div>
                        <div class="form-group">
                            <label for="client_problem">Problem</label>
                            <textarea name="client_problem" id="client_problem" class="form-control" rows="5"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>




            <div id="ticket_result">
                <table>
                    <tr>
                        <td id="results"></td>
                    </tr>
                </table>
            </div>
        </div>


    @endsection

    @section('js')

    @endsection




</x-master>

