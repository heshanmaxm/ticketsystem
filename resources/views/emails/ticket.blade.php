<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Support Ticket Information</title>
</head>
<body>
<p>
    Thank you for contacting our support team. A support ticket has been opened for you.
    You will be notified when a response is made by email. The details of your ticket are shown below:
</p>

<p>Ticket Number: {{ $ticket->ticket_number }}</p>
<p>Problem: {{ $ticket->client_problem }}</p>
<p>Status: {{ $ticket->status }}</p>

<p>

</p>

</body>
</html>
