<x-master>


    @section('content')

        <h1 class="h3 mb-2 text-gray-800">Tickets</h1>


        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">
                    <a href="{{route('tickets.create')}}" class="btn btn-success">Open A New Ticket</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <form method="GET" action="">
                        @csrf
                        <div class="form-group">
                            <label for="getData">Check the status of your Ticket</label>
                            <input type="text" class="form-control" id="getData">
                        </div>

                        <a href="javascript:;" id="search" class="btn btn-primary" >Search</a>
                    </form>
                </div>
                <br>
                <br>





                <table class="table table-responsive">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col"><h4>Ticket Details</h4></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Ticket Number</th>
                        <td id="ticketnumber"></td>
                    </tr>
                    <tr>
                        <th scope="row">Client Email</th>
                        <td id="clientname"></td>
                    </tr>
                    <tr>
                        <th scope="row">Client Problem</th>
                        <td> <p id="clientproblem" style="font-size:1vw;"></p></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td></td>
                    </tr>
                    <thead class="thead-dark">
                    <tr>
                        <th><h4>Ticket Reply</h4></th>
                        <td>
                            <p id="reply"></p>
                        </td>
                    </tr>
                    </thead>
                    </tbody>
                </table>

{{--                <div id="result">--}}
{{--                    <h3 id="title"></h3>--}}
{{--                    <h5 id="ticketnumber"></h5>--}}
{{--                    <p id="clientname"></p>--}}
{{--                    <p id="clientemail"></p>--}}
{{--                    <p id="clientproblem"></p>--}}
{{--                    <h3 id="titlereply"></h3>--}}
{{--                    <p id="reply"></p>--}}
{{--                </div>--}}
            </div>






        </div>


        @endsection

    @section('js')
            <script>
                $(document).ready(function() {
                    $("#search").click(function() {
                        var id = document.getElementById("getData").value;
                        $.ajax({
                            type: "GET",
                            async: false,
                            url: "ticketShow/"+id,
                            dataType: "json",
                            success: function (data) {

                                $('#ticketnumber').html(data.ticket_number);
                                $('#clientname').html(data.client_name);
                                $('#clientemail').html(data.client_email);
                                $('#clientproblem').html(data.client_problem);
                                if(data.reply != null){
                                    $('#reply').html(data.reply.reply);
                                }



                            }
                        });
                    });
                });
            </script>

     @endsection




</x-master>
