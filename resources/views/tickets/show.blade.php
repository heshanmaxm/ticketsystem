<x-master>
    @section('content')
        <h1 class="h3 mb-2 text-gray-800">Ticket</h1>


        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h4 class="m-0 font-weight-bold text-primary"><h3>{{$ticket->ticket_number}}</h3></h4>
            </div>
            <div class="card-body">
                @include('includes.flash')
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th scope="row">Client name</th>
                            <td>{{$ticket->client_name}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Client Email</th>
                            <td>{{$ticket->client_email}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Client Phone</th>
                            <td>{{$ticket->client_phone}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Client Problem</th>
                            <td>{{$ticket->client_problem}}</td>
                        </tr>
                        <tr>
                            <th scope="row"></th>
                            <td><h4>Replies to Client</h4></td>
                        </tr>

                        @foreach($ticket->reply as $reply)
                            <tr>
                                <th></th>
                                <td>{{$reply->reply}}</td>
                            </tr>
                            @endforeach



                        <tr>

                            <th>Reply</th>
                            <td>
                                <form method="post" action="{{ route('tickets.update', $ticket->id) }}">
                                    @method('PATCH')
                                    @csrf
                                    <div class="form-group">
                                        <textarea name="reply" id="reply" class="form-control" rows="10"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-success">Reply</button>
                                    <a href="{{ route('tickets.edit',$ticket->id)}}" class="btn btn-danger">Close ticket</a>
                                </form>

                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="card shadow mb-4">


        </div>


    @endsection

    @section('js')

    @endsection
</x-master>
