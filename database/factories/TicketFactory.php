<?php

namespace Database\Factories;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_name'          => $this->faker->name,
            'client_problem' => Str::random(100),
            'client_email'    => $this->faker->unique()->safeEmail,
            'client_phone' => $this->faker->phoneNumber,
            'ticket_number' => Str::random(10),
            'status' => 'Open',
        ];
    }
}
