<?php
namespace App\Mailers;

use App\Models\Ticket;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer
{
    protected $mailer;
    protected $fromAddress = 'support@supportticket.com';
    protected $fromName = 'Support Ticket';
    protected $to;
    protected $subject;
    protected $view;
    protected $data = [];

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendTicketInformation($useremail, Ticket $ticket)
    {
        $this->to = $useremail;
        $this->subject = "[Ticket No: $ticket->ticket_number]";
        $this->view = 'emails.ticket';
        $this->data = compact('ticket');

        return $this->deliver();
    }

    public function sendTicketReply($reply, Ticket $ticket)
    {
        $this->to = $ticket->client_email;
        $this->subject = "RE: $ticket->ticket_number";
        $this->view = 'emails.ticket_reply';
        $this->data = compact( 'ticket', 'reply');

        return $this->deliver();
    }

    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function ($message) {
            $message->from($this->fromAddress, $this->fromName)
                ->to($this->to)->subject($this->subject);
        });
    }
}
