<?php

namespace App\Http\Controllers;

use App\Mailers\AppMailer;

use App\Models\Reply;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tickets = Ticket::where('status', '=', 'Open')->get();
        return view('tickets.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('newticket');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AppMailer $mailer)
    {
        //
        $this->validate($request, [
            'client_name'           => 'required',
            'client_problem'        => 'required',
            'client_email'          => 'required|email',
            'client_phone'          => 'required',
        ]);

        $useremail = $request->input('client_email');

        $ticket = Ticket::create([
            'client_name'          => $request->input('client_name'),
            'client_problem'       => $request->input('client_problem'),
            'client_email'         => $useremail,
            'client_phone'         => $request->input('client_phone'),
            'ticket_number' => Str::random(10),
            'status'        => "Open",
        ]);



        $mailer->sendTicketInformation($useremail, $ticket);

        return redirect()->back()->with("status", "Your Ticket Number is:  $ticket->ticket_number has been opened.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
        return view('tickets.show', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
        $ticket->update([
            'status' => 'Closed',
        ]);

        $tickets = Ticket::where('status', '=', 'Open')->get();

        return view('tickets.index',compact('tickets'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket, AppMailer $mailer)
    {
        //

        $request->validate([
            'reply' => 'required',
        ]);

        $useremail = $ticket->client_email;

        $reply = new Reply([
            'reply' => $request->input('reply'),
            'ticket_id' => $ticket->id,
        ]);

        $reply->save();

        $mailer->sendTicketReply($reply, $ticket);

        return redirect()->back()->with("status", "Reply has been sent to client");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //

    }

    public function ticketShow($id)
    {

        $ticket = Ticket::where('ticket_number',$id)->first();

        $reply = $ticket->reply;
        return response()->json($ticket);
    }



}
